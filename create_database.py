import psycopg2


# Connect to PostgreSQL DBMS
conn = psycopg2.connect("user=postgres password=postgres")
conn.autocommit = True
cur = conn.cursor()
sql = "CREATE DATABASE test"

try:
    cur.execute(sql)
    print("database created")
except psycopg2.errors.DuplicateDatabase:
    print("database already created")

cur.close()
conn.close()
