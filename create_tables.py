import psycopg2


def create_table():
    sql = "CREATE TABLE IF NOT EXISTS student" \
          "(id serial PRIMARY KEY, name varchar, age integer, grade integer)"
    cur.execute(sql)


def create_second_table():
    sql = "CREATE TABLE IF NOT EXISTS professor" \
          "(id serial PRIMARY KEY, name varchar, age integer)"
    cur.execute(sql)


conn = psycopg2.connect("dbname=test user=postgres password=postgres")
cur = conn.cursor()

create_table()
print("table created")

create_second_table()
print("second table created")

conn.commit()

cur.close()
conn.close()
