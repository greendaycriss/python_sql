# A command line application that does different operations on PostreSQL database.

This project shows how to create SQL queries using psychopg2 PostgreSQL adapter.

## Getting Started

- Run pip install -r requirements.txt
- Run python create_database.py
- Run python create_tables.py
- Run python queries.py



