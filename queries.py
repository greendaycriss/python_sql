import psycopg2


def select_query():
    sql = "SELECT * FROM professor"
    cur.execute(sql)

    print("select output:")
    records = cur.fetchall()
    for x in records:
        print(x)

    return records


def insert_query():
    sql = "INSERT INTO student (name, age) VALUES (%s, %s)"
    val = ("Cristian", 25)
    cur.execute(sql, val)


def second_insert_query():
    sql = "INSERT INTO professor (name, age) VALUES (%s, %s)"
    val = ("Candulet", 25)
    cur.execute(sql, val)


def join_query():
    sql = "SELECT student.name, professor.name FROM student " \
          "INNER JOIN professor ON student.age=professor.age"
    cur.execute(sql)
    print("inner join output:")
    records = cur.fetchall()
    for x in records:
        print(x)


conn = psycopg2.connect("dbname=test user=postgres password=postgres")
cur = conn.cursor()

insert_query()
print("insert executed")

second_insert_query()
print("second insert executed")

select_query()

join_query()

conn.commit()

cur.close()
conn.close()
